# ARCHIVED. DO NOT USE ⚠️⚠️⚠️🗑🗑🗑

This project has been deprecated and is no longer actively maintained. Please use the [asdf-gl-infra](https://gitlab.com/gitlab-com/gl-infra/asdf-gl-infra) plugin going forward to install and manage jsonnet-tool using **asdf**.

![Stop It](http://www.reactiongifs.com/wp-content/uploads/2013/06/stop-it.gif)

# asdf-jsonnet-tool

As [asdf](https://github.com/asdf-vm/asdf) plugin for
[jsonnet-tool](https://gitlab.com/gitlab-com/gl-infra/jsonnet-tool).

## Installation

```
asdf plugin add jsonnet-tool https://gitlab.com/gitlab-com/gl-infra/asdf-jsonnet-tool.git
```

This plugin installs a binary named `jsonnet-tool`.

Requires:

* curl
* jq
